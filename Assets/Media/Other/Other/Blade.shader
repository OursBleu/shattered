// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:4013,x:33451,y:32716,varname:node_4013,prsc:2|diff-5113-OUT;n:type:ShaderForge.SFN_Color,id:1304,x:32482,y:32653,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_1304,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Tex2d,id:5987,x:31854,y:33063,ptovrint:False,ptlb:node_5987,ptin:_node_5987,varname:node_5987,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:0f3c43f72337876418c4123dbcae6ea2,ntxv:0,isnm:False|UVIN-4363-UVOUT;n:type:ShaderForge.SFN_Step,id:9923,x:32108,y:33063,varname:node_9923,prsc:2|A-5262-OUT,B-5987-RGB;n:type:ShaderForge.SFN_Slider,id:5262,x:31677,y:32858,ptovrint:False,ptlb:node_5262,ptin:_node_5262,varname:node_5262,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.01,max:0.01;n:type:ShaderForge.SFN_TexCoord,id:4363,x:31400,y:33170,varname:node_4363,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_ComponentMask,id:8655,x:31676,y:33360,varname:node_8655,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-4363-UVOUT;n:type:ShaderForge.SFN_Abs,id:612,x:32076,y:33360,varname:node_612,prsc:2|IN-8716-OUT;n:type:ShaderForge.SFN_RemapRange,id:8716,x:31849,y:33360,varname:node_8716,prsc:2,frmn:0.4,frmx:0.6,tomn:-1,tomx:1|IN-8655-OUT;n:type:ShaderForge.SFN_Subtract,id:5113,x:33099,y:33134,varname:node_5113,prsc:2|A-164-OUT,B-7021-OUT;n:type:ShaderForge.SFN_OneMinus,id:164,x:32354,y:33106,varname:node_164,prsc:2|IN-9923-OUT;n:type:ShaderForge.SFN_Code,id:7021,x:32521,y:33325,varname:node_7021,prsc:2,code:cgBlAHQAdQByAG4AIAAxACAALQAgACgAMQAtAEEAKQAqACgAMQAtAEEAKQAqACgAMQAtAEEAKQA7AA==,output:8,fname:Function_node_7021,width:408,height:262,input:8,input_1_label:A|A-7614-OUT;n:type:ShaderForge.SFN_Clamp01,id:7614,x:32277,y:33349,varname:node_7614,prsc:2|IN-612-OUT;proporder:1304-5987-5262;pass:END;sub:END;*/

Shader "Shader Forge/Blade" {
    Properties {
        _Color ("Color", Color) = (1,1,1,1)
        _node_5987 ("node_5987", 2D) = "white" {}
        _node_5262 ("node_5262", Range(0, 0.01)) = 0.01
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _node_5987; uniform float4 _node_5987_ST;
            uniform float _node_5262;
            fixed Function_node_7021( fixed A ){
            return 1 - (1-A)*(1-A)*(1-A);
            }
            
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float4 _node_5987_var = tex2D(_node_5987,TRANSFORM_TEX(i.uv0, _node_5987));
                float3 node_9923 = step(_node_5262,_node_5987_var.rgb);
                float node_8716 = (i.uv0.r*9.999999+-5.0);
                float node_612 = abs(node_8716);
                float node_7614 = saturate(node_612);
                float node_7021 = Function_node_7021( node_7614 );
                float3 diffuseColor = ((1.0 - node_9923)-node_7021);
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _node_5987; uniform float4 _node_5987_ST;
            uniform float _node_5262;
            fixed Function_node_7021( fixed A ){
            return 1 - (1-A)*(1-A)*(1-A);
            }
            
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float4 _node_5987_var = tex2D(_node_5987,TRANSFORM_TEX(i.uv0, _node_5987));
                float3 node_9923 = step(_node_5262,_node_5987_var.rgb);
                float node_8716 = (i.uv0.r*9.999999+-5.0);
                float node_612 = abs(node_8716);
                float node_7614 = saturate(node_612);
                float node_7021 = Function_node_7021( node_7614 );
                float3 diffuseColor = ((1.0 - node_9923)-node_7021);
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
