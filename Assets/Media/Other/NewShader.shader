// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:4013,x:33008,y:32662,varname:node_4013,prsc:2|emission-9590-OUT,voffset-3559-OUT;n:type:ShaderForge.SFN_TexCoord,id:1332,x:30817,y:32808,varname:node_1332,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_ComponentMask,id:5174,x:31089,y:32808,varname:node_5174,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-1332-V;n:type:ShaderForge.SFN_Color,id:3724,x:32515,y:32556,ptovrint:False,ptlb:node_3724,ptin:_node_3724,varname:node_3724,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Color,id:9207,x:32515,y:32355,ptovrint:False,ptlb:node_3724_copy,ptin:_node_3724_copy,varname:_node_3724_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Lerp,id:9590,x:32721,y:32581,varname:node_9590,prsc:2|A-9207-RGB,B-3724-RGB,T-3152-OUT;n:type:ShaderForge.SFN_Slider,id:7527,x:31272,y:32755,ptovrint:False,ptlb:Number of gradients,ptin:_Numberofgradients,varname:node_7527,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:5.7372,max:10;n:type:ShaderForge.SFN_Clamp01,id:3152,x:32412,y:32755,varname:node_3152,prsc:2|IN-9127-OUT;n:type:ShaderForge.SFN_Sin,id:9291,x:31864,y:32866,varname:node_9291,prsc:2|IN-2064-OUT;n:type:ShaderForge.SFN_Multiply,id:2064,x:31636,y:32866,varname:node_2064,prsc:2|A-7527-OUT,B-7336-OUT,C-2601-OUT;n:type:ShaderForge.SFN_RemapRange,id:9127,x:32053,y:32830,varname:node_9127,prsc:2,frmn:-1,frmx:1,tomn:0,tomx:1|IN-9291-OUT;n:type:ShaderForge.SFN_Tau,id:2601,x:31415,y:33072,varname:node_2601,prsc:2;n:type:ShaderForge.SFN_Add,id:7336,x:31351,y:32904,varname:node_7336,prsc:2|A-5174-OUT,B-895-OUT;n:type:ShaderForge.SFN_Time,id:4370,x:30844,y:33047,varname:node_4370,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:9657,x:32055,y:33306,prsc:2,pt:False;n:type:ShaderForge.SFN_Slider,id:5553,x:31799,y:33111,ptovrint:False,ptlb:Height,ptin:_Height,varname:node_5553,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0.4295101,max:0.5;n:type:ShaderForge.SFN_Multiply,id:3559,x:32386,y:33001,varname:node_3559,prsc:2|A-9127-OUT,B-5553-OUT,C-9657-OUT;n:type:ShaderForge.SFN_Multiply,id:895,x:31170,y:33028,varname:node_895,prsc:2|A-4370-TSL,B-468-OUT;n:type:ShaderForge.SFN_Slider,id:468,x:30765,y:33282,ptovrint:False,ptlb:Speed,ptin:_Speed,varname:node_468,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:2.073807,max:10;n:type:ShaderForge.SFN_FragmentPosition,id:6114,x:30855,y:32644,varname:node_6114,prsc:2;proporder:3724-9207-7527-5553-468;pass:END;sub:END;*/

Shader "Shader Forge/NewShader" {
    Properties {
        _node_3724 ("node_3724", Color) = (0,0,0,1)
        _node_3724_copy ("node_3724_copy", Color) = (1,0,0,1)
        _Numberofgradients ("Number of gradients", Range(0, 10)) = 5.7372
        _Height ("Height", Range(0, 0.5)) = 0.4295101
        _Speed ("Speed", Range(0, 10)) = 2.073807
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _node_3724;
            uniform float4 _node_3724_copy;
            uniform float _Numberofgradients;
            uniform float _Height;
            uniform float _Speed;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                UNITY_FOG_COORDS(2)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_4370 = _Time;
                float node_9127 = (sin((_Numberofgradients*(o.uv0.g.r+(node_4370.r*_Speed))*6.28318530718))*0.5+0.5);
                v.vertex.xyz += (node_9127*_Height*v.normal);
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
////// Lighting:
////// Emissive:
                float4 node_4370 = _Time;
                float node_9127 = (sin((_Numberofgradients*(i.uv0.g.r+(node_4370.r*_Speed))*6.28318530718))*0.5+0.5);
                float3 emissive = lerp(_node_3724_copy.rgb,_node_3724.rgb,saturate(node_9127));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Back
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float _Numberofgradients;
            uniform float _Height;
            uniform float _Speed;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float4 node_4370 = _Time;
                float node_9127 = (sin((_Numberofgradients*(o.uv0.g.r+(node_4370.r*_Speed))*6.28318530718))*0.5+0.5);
                v.vertex.xyz += (node_9127*_Height*v.normal);
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 normalDirection = i.normalDir;
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
