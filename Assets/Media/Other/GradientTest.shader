// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:4013,x:33271,y:32653,varname:node_4013,prsc:2|emission-2403-OUT;n:type:ShaderForge.SFN_TexCoord,id:435,x:31928,y:33005,varname:node_435,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Color,id:5051,x:32557,y:32326,ptovrint:False,ptlb:node_5051,ptin:_node_5051,varname:node_5051,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Color,id:1767,x:32557,y:32538,ptovrint:False,ptlb:node_5051_copy,ptin:_node_5051_copy,varname:_node_5051_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.7941176,c2:0.291955,c3:0.291955,c4:1;n:type:ShaderForge.SFN_Lerp,id:2403,x:33102,y:32519,varname:node_2403,prsc:2|A-5051-RGB,B-1767-RGB,T-7325-OUT;n:type:ShaderForge.SFN_ComponentMask,id:5162,x:32566,y:32775,varname:node_5162,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-435-UVOUT;n:type:ShaderForge.SFN_Length,id:6170,x:32566,y:32942,varname:node_6170,prsc:2|IN-4486-OUT;n:type:ShaderForge.SFN_RemapRange,id:4486,x:32138,y:33087,varname:node_4486,prsc:2,frmn:0,frmx:1,tomn:-1,tomx:1|IN-435-UVOUT;n:type:ShaderForge.SFN_ArcTan2,id:7325,x:32566,y:33112,varname:node_7325,prsc:2,attp:2|A-6776-G,B-6776-R;n:type:ShaderForge.SFN_ComponentMask,id:6776,x:32342,y:33112,varname:node_6776,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-4486-OUT;proporder:5051-1767;pass:END;sub:END;*/

Shader "Shader Forge/NewShader" {
    Properties {
        _node_5051 ("node_5051", Color) = (0,0,0,1)
        _node_5051_copy ("node_5051_copy", Color) = (0.7941176,0.291955,0.291955,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _node_5051;
            uniform float4 _node_5051_copy;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                UNITY_FOG_COORDS(1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float2 node_4486 = (i.uv0*2.0+-1.0);
                float2 node_6776 = node_4486.rg;
                float3 emissive = lerp(_node_5051.rgb,_node_5051_copy.rgb,((atan2(node_6776.g,node_6776.r)/6.28318530718)+0.5));
                float3 finalColor = emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
