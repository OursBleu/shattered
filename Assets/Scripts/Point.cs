﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Point : MonoBehaviour
{
    public Enemy _enemy;

    [ContextMenu("Bind to parent")]
    private void Bind()
    {
        Transform target = transform;

        int i = 0;
        while (i < 10 && target != null && _enemy == null)
        {
            _enemy = target.GetComponent<Enemy>();
            target = target.parent;
            i++;
        }
    }

    public abstract void OnContact();
}
