﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

public class Sword : MonoBehaviour
{
    public Transform _end;
    public float _reformTime = .5f;
    public float _armorTime = .5f;
    public ShatterManager _shatterManager;
    public SFXManager _sfx;
    public float _minCuttableObjectSize = .1f;

    public IInputManager _input;

    float _length;
    Vector3 _previousPos;

    SwordHit _swordHit = new SwordHit();
    FSM _fsm;

    public void AddHitListener(Action<SwordHit> listener)
    {
        _swordHit.OnHit += listener;
    }
    
    void Awake()
    {
        _input = GetComponent<IInputManager>();

        _length = Vector3.Distance(transform.position, _end.position);
        _previousPos = transform.forward;

        // states

        var full = new ExtensibleState("full");
        full.OnStateUpdate += UpdateSwordHit;

        var armor = new ExtensibleState("armor");
        armor.OnStateUpdate += UpdateSwordHit;

        var broken = new ExtensibleState("broken");
        broken.AddStartSFX(_sfx, SFX.Edited_Shatter_204777__ngruber__breakingglass);
        broken.AddEndSFX(_sfx, SFX.Edited_BladeReady_387957__chrillz3r__metalsound9knifeunsheathed);
        broken.OnStateEnter += () => SetBroken(true);
        broken.OnStateExit += () => SetBroken(false);

        // transitions

        full.Transitions.Add(new Transition(broken, () => _swordHit.HasHit && _swordHit.EnemyPart == null));
        full.Transitions.Add(new Transition(armor, () => _swordHit.HasHit && _swordHit.EnemyPart != null));

        armor.Transitions.Add(new Transition(full, () => _fsm.StateTime > _armorTime));

        broken.Transitions.Add(new Transition(full, () => _fsm.StateTime > _reformTime || _input.GetTriggerDown()));

        // fsm

        _fsm = new FSM(full);
        _fsm.DebugMode = true;
    }

    void Update()
    {
        // store previous pos to get delta next frame

        _previousPos = transform.forward;

        // fsm update

        _fsm.Update(Time.deltaTime);
    }

    void UpdateSwordHit()
    {
        // do multiple raycasts to cover distance between frames

        _swordHit.Reset();
        RaycastHit hit;

        Vector3 delta = transform.position - _previousPos;

        for (float offset = 0; offset < delta.magnitude; offset += _minCuttableObjectSize)
        {
            Vector3 forward = Vector3.MoveTowards(_previousPos, transform.forward, offset);

            Debug.DrawRay(transform.position, forward * _length, Color.blue);

            if (Physics.Raycast(transform.position, forward, out hit, _length))
            {
                _swordHit.SetHit(hit);
                break;
            }
        }
    }

    void SetBroken(bool broken)
    {
        if (broken)
        {
            _shatterManager.Shatter();
        }
        else
        {
            _shatterManager.Reform();
        }
    }

    public class SwordHit : Debuggable
    {
        public Action<SwordHit> OnHit;

        public bool HasHit { get; private set; }
        public Point EnemyPart { get; private set; }
        public RaycastHit Hit { get; private set; }

        Point PreviousEnemyPart;

        public void Reset()
        {
            HasHit = false;
            PreviousEnemyPart = EnemyPart;
            EnemyPart = null;
        }

        public void SetHit(RaycastHit hit)
        {
            Hit = hit;
            HasHit = true;
            EnemyPart = Hit.collider.GetComponent<Point>();

            if (DebugMode)
                Debug.Log((PreviousEnemyPart != null ? PreviousEnemyPart.name : "null") + "=>" + (EnemyPart != null ? EnemyPart.name : "null"));

            // only apply contact effect if the part was not already hit
            if (EnemyPart != null && (PreviousEnemyPart != EnemyPart))
            {
                if (OnHit != null)
                    OnHit(this);

                EnemyPart.OnContact();
            }
        }
    }
}

public static class RendererExtensions
{
    public static bool IsVisibleFrom(this Renderer renderer, Camera camera)
    {
        Plane[] planes = GeometryUtility.CalculateFrustumPlanes(camera);
        return GeometryUtility.TestPlanesAABB(planes, renderer.bounds);
    }
}