﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Healthbar : MonoBehaviour
{
    public Image _image;
    public float _speed = 10f;

    float _current = 1f;
    float _target = 1f;
    float _ratio;

    public void Refresh(int currentHealth, int maxHealth, bool instant = false)
    {
        _current = _image.fillAmount;
        _target = (float)currentHealth / maxHealth;
        _ratio = 0f;

        if (instant)
            _image.fillAmount = _target;
    }

    private void Update()
    {
        if (!Mathf.Approximately(_image.fillAmount, _target))
        {
            _image.fillAmount = Mathf.Lerp(_current, _target, _ratio);
            _ratio += Time.deltaTime * _speed;
        }
    }
}
