﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Player : MonoBehaviour
{
    public float _lungeDistance = 3f;
    public float _lungeDuration = .2f;
    
    void Update ()
    {
		if (Input.GetButtonDown("Fire1"))
        {
            transform.DOComplete();
            transform.DOMove(transform.forward * _lungeDistance, _lungeDuration).SetRelative().SetLoops(2, LoopType.Yoyo);
        }
	}
}
