﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using UnityEngine.UI;

public class Enemy : MonoBehaviour
{
    public int _maxHealth = 3;
    public float _chargingTime = 1f;
    public float _exposedTime = 1f;
    public float _stopDistance = 5f;
    public float _speed = 2f;
    public Healthbar _healthBar;
    public Image _chargingBar;
    public Animator _animator;

    [HideInInspector]
    public SFXManager _sfx;

    int _health;
    float _chargingElapsed;
    int laneID;
    GameObject WaveObject;
    Wave waveScript;

    Action OnDamaged;
    Action OnCounter;
    FSM _fsm;

    void Start()
    {
        WaveObject = GameObject.Find("Wave");
        waveScript = WaveObject.GetComponent<Wave>();
    }

    private void Awake()
    {
        _health = _maxHealth;
        _healthBar.Refresh(_health, _maxHealth, true);
        _chargingBar.fillAmount = 0f;
        _sfx = GameObject.FindObjectOfType<SFXManager>();

        // states

        var approaching = new ExtensibleState("approaching");
        approaching.AddAnimation(_animator, "Idle");
        approaching.OnStateUpdate += () =>
        {
            transform.position += _speed * Time.deltaTime * transform.forward;
        };

        var idle = new ExtensibleState("idle");
        idle.AddAnimation(_animator, "Idle");
        idle.OnStateUpdate += () =>
        {
            _chargingElapsed += Time.deltaTime; //TODO change deltatime
            _chargingBar.fillAmount = _chargingElapsed / _chargingTime;
        };
        idle.OnStateExit += () =>
        {
            _chargingElapsed = 0f;
            _chargingBar.fillAmount = 0f;
        };

        var attack = new ExtensibleState("attack");
        attack.AddAnimation(_animator, "Attack", idle);
        attack.AddStartSFX(_sfx, SFX.Edited_EnemySwoosh_243566__sonictechtonic__airywhooshrighttoleft);

        var exposed = new ExtensibleState("exposed");
        exposed.AddAnimation(_animator, "Exposed");
        exposed.AddStartSFX(_sfx, SFX.Parry2_170964__timgormly__metalping1);
        exposed.AddStartSFX(_sfx, SFX.WeakPointExposed_107789__leviclaassen__hit002);
        exposed.OnStateEnter += () =>
        {
            Vector3 startPos = transform.position;
            transform.DOShakePosition(1f, 0.1f, 60).OnComplete(() => transform.position = startPos);
        };

        var death = new ExtensibleState("death");
        death.AddAnimation(_animator, "Death", null, () => Destroy(gameObject));

        // transitions

        approaching.Transitions.Add(new Transition(idle, () => transform.position.magnitude <= _stopDistance));
        exposed.Transitions.Add(new Transition(idle, () => _fsm.StateTime >= _exposedTime));
        idle.Transitions.Add(new Transition(attack, () => _chargingElapsed >= _chargingTime));
        OnCounter += attack.TransitTo(exposed);

        Transition deathTransition = new Transition(death, () => _health <= 0);
        approaching.Transitions.Add(deathTransition);
        idle.Transitions.Add(deathTransition);
        attack.Transitions.Add(deathTransition);
        exposed.Transitions.Add(deathTransition);
        
        // fsm start

        _fsm = new FSM(approaching);
        //_fsm.DebugMode = true;
    }

    void Update()
    {
        _fsm.Update(Time.deltaTime);
    }

    public void Damage(int amount)
    {
        if (OnDamaged != null)
            OnDamaged();
        
        _health -= amount;
        _healthBar.Refresh(_health, _maxHealth);
        
        if (amount > 1)
            _sfx.Play(SFX.Edited_Critical_Hit_35213__abyssmal__slashkut);
        else
            _sfx.Play(SFX.Edited_Hit_35213__abyssmal__slashkut);
    }

    public void Counter()
    {
        if (OnCounter != null)
            OnCounter();
    }

    public void Slow(float duration)
    {
        if (_fsm.CurrentState.Name == "idle") //TODO do not check for state
        {
            _chargingElapsed -= duration;
            _chargingElapsed = Mathf.Clamp(_chargingElapsed, 0, _chargingTime);
        }
    }

    public void setLaneId(int lane)
    {
        laneID = lane;
    }

    public void OnDestroy()
    {
        waveScript.FreeLane(laneID);
    }
}
