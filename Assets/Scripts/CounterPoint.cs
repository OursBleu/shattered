﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CounterPoint : Point
{
    public override void OnContact()
    {
        _enemy.Counter();
    }
}
