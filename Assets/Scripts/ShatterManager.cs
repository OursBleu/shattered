﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ShatterManager : MonoBehaviour
{
    public float _reformDuration = .5f;
    public float _fragmentSpeed = .01f;
    public GameObject _whole;
    public GameObject _shattered;

    Rigidbody[] _fragments;
    Vector3[] _initalPositions;
    Quaternion[] _initialRotations;
    Vector3[] _fragmentsOldPos;

    bool _isShattered = false;

    private void Awake()
    {
        _fragments = _shattered.GetComponentsInChildren<Rigidbody>();
        _initalPositions = _fragments.Select(x => x.transform.localPosition).ToArray();
        _initialRotations = _fragments.Select(x => x.transform.localRotation).ToArray();
        _fragmentsOldPos = _fragments.Select(x => x.transform.position).ToArray();
    }

    private void LateUpdate()
    {
        _fragmentsOldPos = _fragments.Select(x => x.transform.position).ToArray();
    }

    public void Shatter()
    {
        if (!_isShattered)
        {
            _isShattered = true;
            _whole.SetActive(false);
            _shattered.SetActive(true);

            // unparent and apply some speed to fragments

            for (int i = 0; i < _fragments.Length; i++)
            {
                _fragments[i].transform.parent = null;
                _fragments[i].velocity = (_fragments[i].position - _fragmentsOldPos[i]).normalized * _fragmentSpeed;
            }
        }
    }

    public void Reform()
    {
        if (_isShattered)
        {
            StartCoroutine(InternalReform());
        }
    }

    IEnumerator InternalReform()
    {

        // every fragment should be kinematic before manually setting pos

        for (int i = 0; i < _fragments.Length; i++)
        {
            _fragments[i].isKinematic = true;
        }

        // lerp all fragments to initial pos

        Vector3[] startPos = _fragments.Select(x => x.transform.localPosition).ToArray();
        Quaternion[] startRot = _fragments.Select(x => x.transform.localRotation).ToArray();
        
        for (float elapsed = 0; elapsed < _reformDuration; elapsed += Time.deltaTime)
        {
            for (int i = 0; i < _fragments.Length; i++)
            {
                _fragments[i].transform.position = Vector3.Lerp(startPos[i], _shattered.transform.TransformPoint(_initalPositions[i]), elapsed / _reformDuration);
                _fragments[i].transform.rotation = Quaternion.Lerp(startRot[i], _initialRotations[i] * _shattered.transform.rotation, elapsed / _reformDuration);
            }

            yield return null;
        }

        // reparent fragments

        for (int i = 0; i < _fragments.Length; i++)
        {
            _fragments[i].transform.parent = _shattered.transform;
        }

        // make sure fragments are at initial pos in case the lerp ended a bit off

        for (float elapsed = 0; elapsed < _reformDuration; elapsed += Time.deltaTime)
        {
            for (int i = 0; i < _fragments.Length; i++)
            {
                _fragments[i].transform.localPosition = _initalPositions[i];
                _fragments[i].transform.localRotation = _initialRotations[i];
            }
        }

        // make the fragments dynamic again so physic can affect them

        for (int i = 0; i < _fragments.Length; i++)
        {
            _fragments[i].isKinematic = false;
        }

        // activate the whole blade and disable the fragmented one

        _isShattered = false;
        _whole.SetActive(true);
        _shattered.SetActive(false);
    }
}
