﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankDeath : MonoBehaviour {

    GameObject WaveObject;
    Wave waveScript;

	// Use this for initialization
	void Start () {
        WaveObject = GameObject.Find("Wave");
        waveScript = WaveObject.GetComponent<Wave>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnDestroy() {
        waveScript.TankKilled();
    }
}
