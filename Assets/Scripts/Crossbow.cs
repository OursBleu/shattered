﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Crossbow : MonoBehaviour
{
    public float _chargingTime = 3f;
    public float _slowDuration = 3f;
    public Image _chargingBar;
    public LineRenderer _renderer;

    IInputManager _input;
    FSM _fsm;

    void Awake ()
    {
        _input = GetComponent<IInputManager>();

        var charged = new ExtensibleState("charged");
        charged.OnStateExit += Fire;

        var charging = new ExtensibleState("charging");

        charged.Transitions.Add(new Transition(charging, _input.GetTriggerDown));

        charging.Transitions.Add(new Transition(charged, () => _fsm.StateTime >= _chargingTime));
        charging.OnStateUpdate += () =>
        {
            _chargingBar.fillAmount = _fsm.StateTime / _chargingTime;
        };

        _fsm = new FSM(charged);
        _fsm.DebugMode = true;
    }

    private void Update()
    {
        _fsm.Update(Time.deltaTime);
    }
 
	void Fire ()
    {
        RaycastHit hit;
        Color color = Color.blue;

        if (Physics.Raycast(transform.position, transform.forward, out hit, 500f))
        {
            var enemy = GetEnemy(hit.transform);
            enemy.Slow(_slowDuration);
            color = Color.red;
        }

        //Debug.DrawRay(transform.position, transform.forward * 500f, color, 1f);

        // line renderer

        StartCoroutine(DisplayLaser());
    }

    IEnumerator DisplayLaser()
    {
        _renderer.transform.parent = null;
        _renderer.SetPosition(0, transform.position);
        _renderer.SetPosition(1, transform.position + transform.forward * 500f);
        _renderer.enabled = true;

        yield return new WaitForSeconds(.3f);

        _renderer.enabled = false;
        _renderer.transform.parent = transform;
    }

    Enemy GetEnemy(Transform target)
    {
        Enemy enemy = null;

        int i = 0;
        while (i < 100 && target != null && enemy == null)
        {
            enemy = target.GetComponent<Enemy>();
            target = target.parent;
            i++;
        }

        return enemy;
    }
}
