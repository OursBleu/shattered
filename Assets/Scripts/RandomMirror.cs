﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMirror : MonoBehaviour {

    public int chanceToFlip = 50;
    Vector3 initialScale;

    // Use this for initialization
    void Start () {
        if (Random.Range(0, 100) < chanceToFlip)
        {
            initialScale = transform.localScale;
            transform.localScale = new Vector3(initialScale.x * -1, initialScale.y, initialScale.z);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
