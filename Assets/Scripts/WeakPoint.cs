﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeakPoint : Point
{
    public int _damage = 1;
    
    public override void OnContact()
    {
        _enemy.Damage(_damage);
    }
}
