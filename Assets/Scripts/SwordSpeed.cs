﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordSpeed : MonoBehaviour
{
    public Sword _sword;
    public SpeedCounter _prefab;

    public Transform tip;
    public Transform hilt;

    Vector3 oldTip;
    Vector3 oldHilt;

    void Start()
    {
        _sword.AddHitListener(hit => GetImpactSpeedVector(hit));
    }

    void LateUpdate()
    {
        oldTip = tip.position;
        oldHilt = hilt.position;
    }

    Vector3 GetImpactSpeedVector(Sword.SwordHit hit)
    {
        float hiltToPoint = hit.Hit.distance;
        float hiltToTip = Vector3.Distance(tip.transform.position, hilt.transform.position);

        Vector3 tipDelta = tip.position - oldTip;
        Vector3 hiltDelta = hilt.position - oldHilt;

        Vector3 pointDelta = Vector3.Lerp(hiltDelta, tipDelta, hiltToPoint / hiltToTip);

        Debug.Log(pointDelta.magnitude);
        var counter = Instantiate(_prefab); //TODO: move outside
        counter.transform.position = hit.Hit.point;
        counter.Initialize(pointDelta.magnitude / Time.deltaTime);

        return pointDelta;
    }
}
