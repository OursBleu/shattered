﻿#if UNITY_EDITOR
using UnityEditor;
#endif

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;

public class SFXManager : SceneResources<AudioClip>
{
    public void Play(SFX name)
    {
        AudioSource.PlayClipAtPoint(Get(name), Vector3.zero);
    }
    
    protected override string GetFilter()
    {
        return @"^.*\.(wav|mp3)$";
    }
}
