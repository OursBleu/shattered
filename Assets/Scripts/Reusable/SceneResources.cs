﻿#if UNITY_EDITOR
using UnityEditor;
#endif

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;

public abstract class SceneResources<T> : MonoBehaviour where T : UnityEngine.Object 
{
    public string path = "Media/Sounds/Music";
    public T[] resources;

#if UNITY_EDITOR
    [ContextMenu("Refill")]
    void Refill ()
    {
        string[] files = Directory.GetFiles(Application.dataPath + "/" + path).Where(x => Regex.IsMatch(x, GetFilter())).ToArray();
        Debug.Log(Application.dataPath + "/" + path);
        resources = new T[files.Length];
        for (int i = 0; i < files.Length; i++)
        {
            string assetPath = "Assets" + files[i].Replace(Application.dataPath, "").Replace('\\', '/');
            resources[i] = (T)AssetDatabase.LoadAssetAtPath(assetPath, typeof(T));
        }
        
        GenerateEnum(path, resources.Select(x => string.Join("_", Regex.Replace(x.name, @"(^\d*)|(-)", string.Empty).Split(new char[] { ' ' }, System.StringSplitOptions.RemoveEmptyEntries))).ToArray());
    }

    void GenerateEnum(string filePath, string[] enumEntries)
    {
        string filePathAndName = "Assets/" + filePath + ".cs"; //The folder Scripts/Enums/ is expected to exist
        string enumName = Path.GetFileNameWithoutExtension(filePathAndName);

        using (StreamWriter streamWriter = new StreamWriter(File.Open(filePathAndName, FileMode.Create)))
        {
            streamWriter.WriteLine("public enum " + enumName);
            streamWriter.WriteLine("{");
            for (int i = 0; i < enumEntries.Length; i++)
            {
                streamWriter.WriteLine("\t" + enumEntries[i] + ",");
            }
            streamWriter.WriteLine("}");
        }
        AssetDatabase.Refresh();
    }
#endif
    
    public T Get<TEnum>(TEnum name) where TEnum : struct, System.IComparable, System.IFormattable, System.IConvertible
    {
        return resources[(int)(object)name];
    }

    protected virtual string GetFilter()
    {
        return @"^.*\.(mp3)$";
    }
}
