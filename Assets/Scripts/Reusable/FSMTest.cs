﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FSMTest : MonoBehaviour
{
    FSM fsm;

    private void Awake()
    {
        //var idle = new AnimState(GetComponent<Animation>(), "Idle");
        //var attack = new AnimState(GetComponent<Animation>(), "Attack", idle);
        //var idle = new AnimatorState(GetComponent<Animator>(), "Idle");
        //var attack = new AnimatorState(GetComponent<Animator>(), "Attack", idle, AnimatorState.Method.SetTrigger);

        //idle.Transitions.Add(new Transition(() => Input.GetKeyDown(KeyCode.A), attack));
        //attack.Transitions.Add(new Transition(() => Input.GetKeyDown(KeyCode.B), idle));

        //fsm = new FSM(idle);
        //fsm.DebugMode = true;
    }

    private void Update()
    {
        fsm.Update(Time.deltaTime);
    }
}

public class Debuggable
{
    public bool DebugMode;
}

public class FSM : Debuggable
{
    public float StateTime { get; private set; }

    public State CurrentState { get; private set; }

    public FSM(State currentState)
    {
        CurrentState = currentState;

        CurrentState.StateEnter();
    }

    public void Update(float deltaTime)
    {
        CheckTransitions();
        CurrentState.StateUpdate();
        StateTime += deltaTime;
    }

    public void CheckTransitions()
    {
        var next = CurrentState.Transitions
            .Where(x => x.ShouldExit())
            .Select(x => x.StateToEnter)
            .FirstOrDefault();
        
        if (next != null)
            ChangeState(next);
    }

    public void ChangeState(State nextState)
    {
        if (DebugMode)
            Debug.Log(CurrentState.Name + "=>" + nextState.Name);

        CurrentState.StateExit();
        CurrentState = nextState;
        StateTime = 0f;
        CurrentState.ResetFlags();
        CurrentState.StateEnter();
    }
}

public class State
{
    public List<Transition> Transitions = new List<Transition>();
    public Dictionary<State, bool> Flags;

    public virtual void StateEnter() { }
    public virtual void StateUpdate() { }
    public virtual void StateExit() { }
    
    public string Name;
    
    public State(string name)
    {
        Name = name;
    }

    // OnLifeEmpty += alive.TransitOnEvent(dead)
    public Action TransitTo(State nextState)
    {
        AddTransitionFlag(nextState);
        return () => SetFlagFor(nextState);
    }

    void AddTransitionFlag(State nextState)
    {
        if (Flags == null)
            Flags = new Dictionary<State, bool>();

        Flags.Add(nextState, false);

        Transitions.Add(new Transition(nextState, () =>
        {
            var res = Flags[nextState];
            Flags[nextState] = false;
            return res;
        }));
    }

    public void ResetFlags()
    {
        if (Flags != null)
            foreach (var key in new List<State>(Flags.Keys))
                Flags[key] = false;
    }
    void SetFlagFor(State nextState)
    {
        Flags[nextState] = true;
    }
}

public class ExtensibleState : State
{
    public Action OnStateEnter;
    public Action OnStateUpdate;
    public Action OnStateExit;

    public ExtensibleState(string name) : base(name)
    {
    }

    public override void StateEnter()
    {
        if (OnStateEnter != null)
            OnStateEnter();
    }

    public override void StateUpdate()
    {
        if (OnStateUpdate != null)
            OnStateUpdate();
    }

    public override void StateExit()
    {
        if (OnStateExit != null)
            OnStateExit();
    }
}

public static class DelegateBasedStateExtensions
{
    public static void AddAnimation(this ExtensibleState state, Animator animator, string name, State endState = null, Action onEnd = null)
    {
        if (endState != null)
        {
            state.Transitions.Add(new Transition(endState, () =>
            {
                var animState = animator.GetCurrentAnimatorStateInfo(0);
                return animState.IsName(name) && animState.normalizedTime > 0.75f;
            }));
        }

        state.OnStateEnter += () => animator.CrossFade(name, 0.25f);

        if (onEnd != null)
        {
            state.OnStateUpdate += () =>
            {
                var animState = animator.GetCurrentAnimatorStateInfo(0);
                if (animState.IsName(name) && animState.normalizedTime >= 1f)
                {
                    onEnd();
                }
            };
        }
    }

    public static void AddStartSFX(this ExtensibleState state, SFXManager sfxManager, SFX sfx)
    {
        state.OnStateEnter += () => sfxManager.Play(sfx);
    }

    public static void AddEndSFX(this ExtensibleState state, SFXManager sfxManager, SFX sfx)
    {
        state.OnStateExit += () => sfxManager.Play(sfx);
    }
}


public class Transition
{
    public delegate bool Condition();

    public Condition ShouldExit;
    public State StateToEnter;

    public Transition(State stateToEnter, Condition exitCondition)
    {
        ShouldExit = exitCondition;
        StateToEnter = stateToEnter;
    }
}

// ----------------------------------------------------------------------- SEQ -------------------------------------------------------------

public abstract class CoroutineState : State
{
    Coroutine _coroutine;
    MonoBehaviour _host;
    IEnumerator _enumerator;

    public CoroutineState(string name, MonoBehaviour host) : base(name)
    {
        _host = host;
    }

    public override void StateEnter()
    {
        _enumerator = Flow();
        _coroutine = _host.StartCoroutine(_enumerator);
    }

    public abstract IEnumerator Flow();

    public override void StateExit()
    {
        _host.StopCoroutine(_coroutine);
    }
}