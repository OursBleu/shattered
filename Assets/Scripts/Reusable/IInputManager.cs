﻿using UnityEngine;

public interface IInputManager
{
    bool GetApplicationMenu();
    Vector2 GetAxis();
    bool GetGripDown();
    bool GetGripUp();
    bool GetTriggerDown();
    bool GetTriggerUp();
}