﻿#if UNITY_EDITOR
using UnityEditor;
#endif

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;
using System.Text.RegularExpressions;

public class MusicManager : SceneResources<AudioClip>
{
    public AudioSource source;
    
    public void Play(Music name)
    {
        source.clip = Get(name);
        source.Play();
    }

    public void Play(int i)
    {
        source.clip = resources[i];
        source.Play();
    }

    protected override string GetFilter()
    {
        return @"^.*\.(mp3)$";
    }
}
