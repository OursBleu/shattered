﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour, IInputManager
{
    public Vector2 GetAxis()
    {
        return new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
    }

    public bool GetTriggerDown()
    {
        return Input.GetKeyDown(KeyCode.K);
    }

    public bool GetTriggerUp()
    {
        return Input.GetKeyUp(KeyCode.K);
    }

    public bool GetGripDown()
    {
        return Input.GetKeyDown(KeyCode.L);
    }

    public bool GetGripUp()
    {
        return Input.GetKeyDown(KeyCode.L);
    }

    public bool GetApplicationMenu()
    {
        return Input.GetKeyDown(KeyCode.M);
    }
}
