﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRInputManager : MonoBehaviour, IInputManager
{
    public SteamVR_TrackedObject trackedObj;

    private SteamVR_Controller.Device Controller
    {
        get { return SteamVR_Controller.Input((int)trackedObj.index); }
    }

    public Vector2 GetAxis()
    {
        return Controller.GetAxis();
    }

    public bool GetTriggerDown()
    {
        return Controller.GetHairTriggerDown();
    }

    public bool GetTriggerUp()
    {
        return Controller.GetHairTriggerUp();
    }

    public bool GetGripDown()
    {
        return Controller.GetPressDown(SteamVR_Controller.ButtonMask.Grip);
    }

    public bool GetGripUp()
    {
        return Controller.GetPressUp(SteamVR_Controller.ButtonMask.Grip);
    }

    public bool GetApplicationMenu()
    {
        return Controller.GetPressUp(SteamVR_Controller.ButtonMask.ApplicationMenu);
    }
}
