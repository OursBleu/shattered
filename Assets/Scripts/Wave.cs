﻿using SubjectNerd.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wave : MonoBehaviour
{
    //Trying a random generation of enemies 
    public int maxConcurrentTanks = 1;
    public float spawnInterval = 3.0f;
    public float intervalDecrease = 0.2f;
    int tankCount = 0;
    public Enemy Tank;
    public Enemy Nuisance;

    [System.Serializable]
    public class SpawnConfig
    {
        [Range(0, 360)]
        public float Angle = 0f;

        [Range(0, 180)]
        public float Time;

        [Range(0, 50)]
        public float Distance;

        public Enemy Prefab;
    }

    [Reorderable]
    public SpawnConfig[] _spawns;

    float _elapsedTime = 0f;
    int _indexOfNextToSpawn = 0;
    bool[] lanes = new bool [8];
    int spawnAngle;
    
    void Start()
    {
        for(int i =0; i < lanes.Length; i++)
        {
            lanes[i] = true;
        }
    }
     
    void Update()
    {
        if (_indexOfNextToSpawn == _spawns.Length)
            _indexOfNextToSpawn = 0;
            //return;

        SpawnConfig spawn = _spawns[_indexOfNextToSpawn];

        //if (spawn.Time <= _elapsedTime)
        if (spawnInterval <= _elapsedTime)
        {
            int spawnAngle = FindSpawnAngle();

            if (spawnAngle == -1) //If there's no lane to spawn the enemy, wait for next round
            {
                _elapsedTime = 0f;
            }
            else
            {
                Enemy enemy;
                if (tankCount < maxConcurrentTanks) //If there's space for a tank, shit one out
                {
                    enemy = Instantiate(Tank);
                    tankCount++;
                }
                else
                {
                    enemy = Instantiate(Nuisance);
                }
                //Enemy enemy = Instantiate(spawn.Prefab);
                //enemy.transform.rotation = Quaternion.AngleAxis(180f + spawn.Angle, Vector3.up); NOTE: Made the spawn angle random
                enemy.transform.rotation = Quaternion.AngleAxis(180f + spawnAngle, Vector3.up);
                Vector3 enemyPos = -enemy.transform.forward * spawn.Distance;
                enemyPos.y = transform.position.y;
                enemy.transform.position = enemyPos;
                enemy.setLaneId(spawnAngle / 45);

                spawnInterval -= intervalDecrease;
                _indexOfNextToSpawn++;
                _elapsedTime = 0f;
            }
        }

        _elapsedTime += Time.deltaTime;
    }

    private int FindSpawnAngle()
    {
        int random = Random.Range(0, lanes.Length);

        for (int i = 0; i < lanes.Length; i++)
        {
            if (lanes[(random + i) % 8] == true)
            {
                lanes[(random + i) % 8] = false;
                return ((random + i) % 8) * 45; //Offset the iterator by the random, then modulo to be sure we're within the array, couldve probable been done cleaner

            }
        }
        return -1; // Means there's no empty lane
  
    }

    // A Tank has been killed, decrease counter 
    public void TankKilled() {
        tankCount--;
    }

    //Any enemy has been killed, free up its lane
    public void FreeLane(int lane)
    {
        lanes[lane] = true;
    }
}
