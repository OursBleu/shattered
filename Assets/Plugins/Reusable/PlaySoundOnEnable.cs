﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnEnable : MonoBehaviour
{
    public AudioSource audioSource;

	void OnEnable()
    {
        audioSource.Play();
    }
}
