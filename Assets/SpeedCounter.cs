﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class SpeedCounter : MonoBehaviour {

    [SerializeField]
    Text _counter;
     
	public void Initialize(float value)
    {
        _counter.text = (value * 1f).ToString("0.00");

        _counter.DOFade(0f, 1f).SetDelay(0.5f).OnComplete(() => Destroy(gameObject));

        transform.forward = -Camera.main.transform.forward;
        transform.position += transform.forward * 0.5f;
    }

    void LateUpdate()
    {
        transform.forward = Camera.main.transform.forward;
    }
}
